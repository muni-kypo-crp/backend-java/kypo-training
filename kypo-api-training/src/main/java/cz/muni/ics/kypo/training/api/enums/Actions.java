package cz.muni.ics.kypo.training.api.enums;

/**
 * The enumeration of Actions.
 *
 */
public enum Actions {

    /**
     * None actions.
     */
    NONE,
    /**
     * Shows results of finished training runs.
     */
    RESULTS,

    /**
     * Resume actions.
     */
    RESUME;
}
