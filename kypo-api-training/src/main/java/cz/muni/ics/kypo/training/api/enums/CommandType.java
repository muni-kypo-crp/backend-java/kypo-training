package cz.muni.ics.kypo.training.api.enums;

/**
 * The enumeration of Command Types.
 */
public enum CommandType {

    /**
     * represents a command used in bash console.
     */
    BASH,
    /**
     * represents a command used in msfconsole.
     */
    MSF;
}
