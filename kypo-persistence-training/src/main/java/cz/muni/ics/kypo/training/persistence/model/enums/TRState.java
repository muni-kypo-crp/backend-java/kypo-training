package cz.muni.ics.kypo.training.persistence.model.enums;

/**
 * States represented in Training Run entity.
 *
 */
public enum TRState {

    /**
     * Running run state.
     */
    RUNNING,
    /**
     * Finished run state.
     */
    FINISHED,
    /**
     * Archived run state.
     */
    ARCHIVED;
}
