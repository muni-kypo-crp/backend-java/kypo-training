package cz.muni.ics.kypo.training.persistence.model.enums;

public enum CheatingDetectionState {

    /**
     * represents a running state.
     */
    QUEUED,
    /**
     * represents a running state.
     */
    RUNNING,
    /**
     * represents a running state.
     */
    DISABLED,
    /**
     * represents a finished state.
     */
    FINISHED;
}
