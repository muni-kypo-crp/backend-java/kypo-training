CREATE SEQUENCE extend_matching_option_seq AS bigint INCREMENT 50 MINVALUE 1;
CREATE SEQUENCE extended_matching_statement_seq AS bigint INCREMENT 50 MINVALUE 1;
CREATE SEQUENCE question_choice_seq AS bigint INCREMENT 50 MINVALUE 1;
CREATE SEQUENCE question_seq AS bigint INCREMENT 50 MINVALUE 1;
